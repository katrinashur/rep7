#include<string>                                                //commit 2
#include<vector>                                                //commit 4
using namespase std;                                            //commit  2

void main()                                                     //commit 1
{                                                               //commit 2
    string str = "Hello world!";                                //commit 3
    string str = "test str";                                    //commit 8
    cout << str << endl;                                        //commit 8
    std::vector<int> numbers{3, 4, 2, 9, 15, 267};              //commit 6
    int sum = sumVector(numbers);                               //commit 7

    cout << vectorToString(numbers);                            //commit 10
}                                                               //commit 2
                                                                       

void printVector(vector<int> numbers)                           //commit 6
{                                                               //commit 4
    for (auto n : numbers) {                                    //commit 6
        std::cout << n << " ";                                  //commit 10
    }                                                           //commit 4
    cout << "Print Vector!"                                     //commit 11
}                                                               //commit 4

string vectorToString(vector<int> numbers)                      //commit 9
{                                                               //commit 9
    string res = "";                                            //commit 9
    for (auto n : numbers) {                                    //commit 9
        res += to_string(n);                                    //commit 9
    }                                                           //commit 9
    cout << "Vector to String!"                                 //commit 11
}

int sumVector(vector<int> numbers)                              //commit 6
{                                                               //commit 6
    int result = 0;                                             //commit 9
    for (auto n : numbers) {                                    //commit 6
        result +=n;                                             //commit 9
    }                                                           //commit 6
    return result;                                              //commit 9
}                                                               //commit 6


                                                                